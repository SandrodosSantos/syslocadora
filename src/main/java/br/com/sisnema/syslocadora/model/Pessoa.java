package br.com.sisnema.syslocadora.model;

public class Pessoa {

	private static int cont;
	private int id;
	private String nome;
	private String enderešo;
	
	public Pessoa () {
		id = ++cont;
	}
	
	public Pessoa(int id) {
		super();
		this.id = id;
	}
	

	@Override
	public String toString() {
		return "Pessoa [id=" + id + ", nome=" + nome + ", enderešo=" + getEnderešo() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Pessoa)) {
			return false;
		}
		Pessoa other = (Pessoa) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEnderešo() {
		return enderešo;
	}

	public void setEnderešo(String enderešo) {
		this.enderešo = enderešo;
	}
	
	
}
