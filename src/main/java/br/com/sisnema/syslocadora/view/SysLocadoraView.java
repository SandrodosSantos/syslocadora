package br.com.sisnema.syslocadora.view;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import br.com.sisnema.syslocadora.model.Pessoa;

public class SysLocadoraView {
        
	
	private static ArrayList<Pessoa> clientes = new ArrayList<>();

	public static void main(String[] args) {
		
		
		int opcao = 0;
		do {
		try {
			
			opcao = Integer.parseInt(JOptionPane.showInputDialog(getMenu()));
			switch (opcao) {
				case 0:
					JOptionPane.showMessageDialog(null, "Sair");
					break;
	
				case 1:
					PessoaView.run(clientes);
					break;
				case 2:
					JOptionPane.showMessageDialog(null, "Em desenvolvimento");
					break;

			default:
				JOptionPane.showMessageDialog(null, "Op��o Inv�lida");
				break;
			}
		} 	catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Valor inv�lido");
				opcao = -1;
		}
		} 	while (opcao != 0);
		
		
	}
	

	public static String getMenu() {
		return "MENU PRINCIPAL\n" + 
				"-----------------------\n" + 
				"1 - Clientes\n" + 
				"2 - Filmes\n" + 
				"3 - Midias\n" + 
				"4 - Gravar em Dis	co\n" + 
				"5 - Ler do Disco\n" + 
				"0 - Sair\n";
		
	}

}
