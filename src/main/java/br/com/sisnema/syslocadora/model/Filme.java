package br.com.sisnema.syslocadora.model;

public class Filme {
 
	public String codigo;
	public String nome;
	public String genero;
	@Override
	public String toString() {
		return "Filme [codigo=" + codigo + ", nome=" + nome + ", genero=" + genero + "]";
	}
	
	
	
}
