package br.com.sisnema.syslocadora.model;

public class Midia {

	public String codigo;
	public String descricao;
	@Override
	public String toString() {
		return "Midia [codigo=" + codigo + ", descricao=" + descricao + "]";
	}
	
}
