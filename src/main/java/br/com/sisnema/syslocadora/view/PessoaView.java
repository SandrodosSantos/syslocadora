package br.com.sisnema.syslocadora.view;

import java.awt.HeadlessException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import br.com.sisnema.syslocadora.model.Pessoa;

public class PessoaView {

	public static void run (ArrayList<Pessoa> clientes) {
		int opcao = 0;
			do {
				try {
					opcao = Integer.parseInt(JOptionPane.showInputDialog(getMenu()));
					switch (opcao) {
						case 0:
							break;
						case 1:
							Pessoa p = cadastrar();
							clientes.add (p);
							break;
						case 2:
							editar(clientes);
						
						break;
						case 3:
							excluir(clientes);
							break;						

						case 4:
						   listar(clientes);
						   	
						   break;
					default:
						JOptionPane.showMessageDialog(null, "Op��o inv�lida");
						break;
					}
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(null, "Valor inv�lido");
					opcao = -1;
				} 
			} while (opcao != 0);
		
		
		
	}
	
	private static void editar(ArrayList<Pessoa> clientes) {
      try {
		int idClienteAhEditar = Integer.parseInt(JOptionPane.showInputDialog("Informe o ID a ser editado"));
		  Pessoa clienteAhEditar = new Pessoa(idClienteAhEditar);
		  int index = clientes.indexOf(clienteAhEditar);
		  
		  if (index >= 0) {
			  clienteAhEditar = clientes.get(index);
			  clienteAhEditar.setNome(JOptionPane.showInputDialog("Nome", clienteAhEditar.getNome()));
			  clienteAhEditar.setEndere�o(JOptionPane.showInputDialog("Endere�o", clienteAhEditar.getEndere�o()));
			  
		  }else {
			  JOptionPane.showMessageDialog(null, "Cliente n�o Encontrado");
   
		  }
	} catch (NumberFormatException e) {
			JOptionPane.showInternalMessageDialog(null, "Valor inv�lido");
			editar(clientes);
	}
      
	}

	private static void excluir (ArrayList<Pessoa> clientes) {
		try {
			int idClientAhExcluir = Integer.parseInt(JOptionPane.showInputDialog("Informe o ID a ser exclu�do"));
			Pessoa clienteAhExcluir = new Pessoa(idClientAhExcluir);
			
			if(clientes.remove(clienteAhExcluir)){
			    JOptionPane.showMessageDialog(null, "Cliente removido com Sucesso");
} else {
			JOptionPane.showMessageDialog(null, "Cliente n�o encontrado");
			
}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Valor Inv�lido");
			excluir (clientes);
			
		}
}

	private static void listar(ArrayList<Pessoa> clientes) {
		try {
			String linhas = "";
			int cont = 0;
			
			
			for (Pessoa pessoa : clientes) {
				linhas += "pessoa["+cont+"]"+pessoa +"\n";
			
			if (++cont % 3 == 0) {
				JOptionPane.showMessageDialog(null, linhas);
				linhas = "";
								}}
			if (linhas.length()>0) {
				JOptionPane.showMessageDialog(null, linhas);	
			}
		} catch (HeadlessException e) {
			JOptionPane.showMessageDialog(null, "Valor Inv�lido");
			
		}
		
		
			}
		
	

	public static String getMenu () {
		return "MENU CLIENTES\n" + 
				"-----------------------\n" + 
				"1 - Cadastrar\n" + 
				"2 - Alterar\n" + 
				"3 - Excluir\n" + 
				"4 - Listar\n" + 
				"0 - Voltar\\n";
	}
	
	public static Pessoa cadastrar () {
		Pessoa variavelpessoa = new Pessoa();
		
	
	
	variavelpessoa.setNome(JOptionPane.showInputDialog("Informe o nome da pessoa"));
	variavelpessoa.setEndere�o(JOptionPane.showInputDialog("Informe o endere�o da pessoa"));
	return variavelpessoa;
	
	
	
		         
			
		
	}
}
	
